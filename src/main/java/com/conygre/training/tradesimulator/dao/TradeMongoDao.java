package com.conygre.training.tradesimulator.dao;

import java.util.List;

import com.conygre.training.tradesimulator.model.TradeRequest;
import com.conygre.training.tradesimulator.model.TradeState;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TradeMongoDao extends MongoRepository<TradeRequest, ObjectId> {

	public List<TradeRequest> findByTradeState(TradeState tradeState);
}