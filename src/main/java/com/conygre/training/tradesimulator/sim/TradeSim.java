package com.conygre.training.tradesimulator.sim;

import java.util.List;

import com.conygre.training.tradesimulator.dao.TradeMongoDao;
import com.conygre.training.tradesimulator.model.TradeRequest;
import com.conygre.training.tradesimulator.model.TradeState;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class TradeSim {
    private static final Logger LOG = LoggerFactory.getLogger(TradeSim.class);

    @Autowired
    private TradeMongoDao tradeDao;

    @Transactional
    public List<TradeRequest> findTradesForProcessing(){
        List<TradeRequest> foundTrades = tradeDao.findByTradeState(TradeState.CREATED);

        for (TradeRequest thisTrade : foundTrades) {
            thisTrade.setTradeState(TradeState.PENDING);
            tradeDao.save(thisTrade);
        }

        return foundTrades;
    }

    @Transactional
    public List<TradeRequest> findTradesForFilling() {
        List<TradeRequest> foundTrades = tradeDao.findByTradeState(TradeState.PENDING);

        for (TradeRequest thisTrade : foundTrades) {
            if ((int) (Math.random() * 10) > 8) {
                thisTrade.setTradeState(TradeState.REJECTED);
            } else {
                thisTrade.setTradeState(TradeState.FILLED);
            }
            tradeDao.save(thisTrade);
        }

        return foundTrades;
    }

  //adds a delay in milli seconds
    @Scheduled(fixedDelay = 30000)
    public void runSim() {
        LOG.info("Main loop running!");

        int tradesForFilling = findTradesForFilling().size();
        LOG.info("Found " + tradesForFilling + " trades to be filled/rejected");

        int tradesForProcessing = findTradesForProcessing().size();
        LOG.info("Found " + tradesForProcessing + " trades to be processed");

    }
}
